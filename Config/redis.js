const { createCluster, createClient } = require('redis');


const connectRedis = () => {
    try {
        const cluster = createCluster({
            rootNodes: [
                {
                    url: `redis://${process.env.REDIS_HOST_1}:${process.env.REDIS_PORT}`
                },
                {
                    url: `redis://${process.env.REDIS_HOST_2}:${process.env.REDIS_PORT}`
                },
                {
                    url: `redis://${process.env.REDIS_HOST_3}:${process.env.REDIS_PORT}`
                },
                {
                    url: `redis://${process.env.REDIS_HOST_4}:${process.env.REDIS_PORT}`
                },
                {
                    url: `redis://${process.env.REDIS_HOST_5}:${process.env.REDIS_PORT}`
                },
                {
                    url: `redis://${process.env.REDIS_HOST_6}:${process.env.REDIS_PORT}`
                }
            ]
        });

        // const cluster = createClient();

        return cluster;

    } catch (error) {
        console.log(error);
    }
}

module.exports = { connectRedis };