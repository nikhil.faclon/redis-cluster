const express = require('express');
const app = express();
const dotenv = require('dotenv').config();
const { connectRedis } = require('./Config/redis');
const { writeInRedis, readFromRedis } = require('./redisScript');

const redisCluster = connectRedis();

// (async () => {
//     await redisCluster.connect();

//     redisCluster.on('connect', () => {
//         console.log('Connected to Redis');
//     })

//     redisCluster.on('error', (error) => {
//         console.log("Error connecting to redis");
//         console.log(error);
//     });
// })();

writeInRedis();
readFromRedis();


const PORT = process.env.PORT || 5001;
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});

