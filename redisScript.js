const { connectRedis } = require('./Config/redis');
const redisCluster = connectRedis();

// console.log(redisCluster);

const writeInRedis = async () => {
    try {
        await redisCluster.connect();
        for (let i = 0; i < 100; i++) {
            await redisCluster.set(`key${i}`, i);
        }
    } catch (error) {
        console.log(error);
    }
};

const readFromRedis = async () => {
    try {
        await redisCluster.connect();
        for (let i = 0; i < 100; i++) {
            const data = await redisCluster.get(`key${i}`);
            console.log(data);
        }
    } catch (error) {
        console.log(error);
    }
};

module.exports = {
    writeInRedis,
    readFromRedis,
};